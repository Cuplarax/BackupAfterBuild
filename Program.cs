﻿using System;
using System.Text;

public static class Code
{
	public static void Main(string[] args)
	{
		{
			//args = new string[] { "E$cs", "N$test09", @"B$E:\LOL_TEST", @"P$C:\Users\1\Desktop\ConsoleWork\ConsoleWork" };
			Console.InputEncoding = Console.OutputEncoding = Encoding.Unicode;
			Console.CursorVisible = false;
			int choice;
			Other.Load(args);
			if (!Other.Check())
				return;
			while (true)
			{
				Other.lines = 0;
				Console.WriteLine("Создать бэкап?");
				choice = Other.Num_menu("создать", "пропустить", "проверить данные");
				if (choice == 3)
				{
					Color.Print("--данные--\n" +
						$"название проекта = {Other.parameters[Parameter.name]}\n" +
						$"папка с исходными файлами = {Other.parameters[Parameter.path]}\n" +
						$"папка для бэкапа = {Other.parameters[Parameter.backup]}\n" +
						$"{(!(Other.parameters[Parameter.ext] is null) ? $"фильтр расширения файлов = .{Other.parameters[Parameter.ext]}\n" : "")}",
						ConsoleColor.Cyan);
					Console.WriteLine("\n\nнажмите любую клавишу, чтобы продолжить");
					Console.ReadKey(true);
					Console.Clear();
				}
				else if (choice == 2)
					break;
				else
				{
					Other.Backup();
					break;
				}
			}
		}
	}
}