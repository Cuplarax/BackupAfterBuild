﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

public enum Parameter
{
	path,
	ext,
	backup,
	name,
	comment
}

public static class Color
{
	public static void Print(string text, ConsoleColor color)
	{
		Console.ForegroundColor = color;
		Console.WriteLine(text);
		Console.ResetColor();
	}
}

public static class Sound
{
	private static void play0(object obj)
	{
		if (obj is bool IsError)
			if (!IsError)
			{
				Console.Beep(2400, 150);
				Console.Beep(3000, 150);
			}
			else
			{
				Console.Beep(1500, 200);
				Console.Beep(1000, 250);
			}
	}
	public static void Play(bool IsError) => new Thread(play0).Start(IsError);
}

public static class Other
{
	public static bool Separate = false;
	public static int lines = 0;
	public static int filled = 0;
	public static int Num_menu(params string[] variants)
	{
		ConsoleKeyInfo answer;
		for (int num = 0; num < variants.Length; num++)
			Color.Print($"{num + 1}. {variants[num]}", ConsoleColor.Yellow);
		while (true)
		{
			answer = Console.ReadKey(true);
			if ((int)answer.Key >= 49 && (int)answer.Key <= 57 - (9 - variants.Length))
			{
				Console.Clear();
				return (int)answer.Key - 48;
			}
		}
	}
	public static void Crash(string message)
	{
		Sound.Play(true);
		Console.Clear();
		Color.Print("произошла ошибка\n" +
			"  |\n" +
			"  =- " + message, ConsoleColor.Red);
		Console.WriteLine("\nнажмите на любую клавишу, чтобы выйти");
		Console.ReadKey(true);
	}
	public static Dictionary<Parameter, string> parameters = new Dictionary<Parameter, string>
		{
			{ Parameter.ext, null },
			{ Parameter.path, null },
			{ Parameter.backup, null },
			{ Parameter.name, null },
			{ Parameter.comment, null }
		};
	public static string new_path;
	public static bool Check()
	{
		string error_text = null;
		foreach (var pair in parameters)
		{
			if (pair.Value is null && pair.Key != Parameter.ext && pair.Key != Parameter.comment)
			{
				switch (pair.Key)
				{
					case Parameter.path:
						error_text = "необходимо указать откуда будут копироваться файлы";
						break;
					case Parameter.backup:
						error_text = "необходимо указать папку куда будут копироваться файлы";
						break;
					case Parameter.name:
						error_text = "необходимо указать название для проекта, файлы которого будут копироваться";
						break;
				}
				Crash(error_text);
				return false;
			}
		}
		return true;
	}
	public static void Load(string[] args)
	{
		string token;
		string value;
		foreach (string arg in args)
		{
			token = arg.Split('$')[0];
			value = arg.Split('$')[1];
			switch (token)
			{
				case "E":
					parameters[Parameter.ext] = value;
					break;
				case "P":
					parameters[Parameter.path] = value.Replace("^^", " ");
					break;
				case "B":
					parameters[Parameter.backup] = value.Replace("^^", " ");
					break;
				case "N":
					parameters[Parameter.name] = value.Replace("^^", " ");
					break;
				case "C":
					parameters[Parameter.comment] = value.Replace("^^", " ");
					Separate = true;
					break;
			}
		}
	}

	private static void Copy(string file)
	{
		string local_file = parameters[Parameter.path] + '\\' + file;
		File.Copy(local_file, new_path + '\\' + file);
		int lines_count = File.ReadAllLines(local_file).Length;
		Interlocked.Add(ref lines, lines_count);
		Color.Print($"файл {file} копирован. {new String(' ', 30 - file.Length)} строк: {lines_count}", ConsoleColor.Cyan);
	}

	private static void GetFilledLines(string file)
	{
		string local_file = parameters[Parameter.path] + '\\' + file;
		string[] lines = File.ReadAllLines(local_file);
		int filledCount = 0;
		foreach (string line in lines)
			if (String.IsNullOrWhiteSpace(line) || (Separate && line.Replace(" ", "").Replace("\t", "").Replace("\0", "").StartsWith(parameters[Parameter.comment]))) { }
			else filledCount++;
		Interlocked.Add(ref filled, filledCount);
	}
	public static void Backup()
	{
		DateTime time = DateTime.Now;
		if (Directory.Exists(parameters[Parameter.path])) { }
		else
		{
			Crash($"директории {parameters[Parameter.path]} - не существует");
			return;
		}
		string[] file_list = Directory.GetFiles(parameters[Parameter.path]);
		List<string> new_list = new List<string> { };
		if (parameters[Parameter.ext] is null) { }
		else
		{
			Color.Print("отбор файлов по определённому расширению", ConsoleColor.Yellow);
			foreach (string file in file_list)
			{
				if (file.Split('.')[1] == parameters[Parameter.ext])
					new_list.Add(file);
			}
			file_list = new_list.ToArray();
			Color.Print("отобрано", ConsoleColor.Green);
		}
		new_list.Clear();
		foreach (string file in file_list)
			new_list.Add(file.Split('\\')[file.Split('\\').Length - 1]);
		file_list = new_list.ToArray();
		List<Task> tasks = new List<Task> { };
		foreach (string file in file_list)
			tasks.Add(new Task(() => GetFilledLines(file)));
		foreach (Task task in tasks)
			task.Start();
		if (!Directory.Exists(parameters[Parameter.backup]))
		{
			Directory.CreateDirectory(parameters[Parameter.backup]);
			Color.Print("несуществующая директория была создана", ConsoleColor.Yellow);
		}
		new_path = $"{parameters[Parameter.backup]}\\{parameters[Parameter.name]} {time.Year}.{time.Month}{time.Day} {time.Hour}.{time.Minute}.{time.Second}";
		if (Directory.Exists(new_path))
		{
			Directory.Delete(new_path, true);
			Color.Print($"существующий каталог был удалён", ConsoleColor.Yellow);
		}
		Directory.CreateDirectory(new_path);
		Color.Print("директория для бэкапа создана", ConsoleColor.Green);
		foreach (string file in file_list) Copy(file);
		Task.WaitAll(tasks.ToArray());
		Color.Print("--бэкап создан--", ConsoleColor.Green);
		Color.Print($"папка с бэкапом = {new_path}", ConsoleColor.Cyan);
		Color.Print($"количество строк = {lines}", ConsoleColor.Cyan);
		if (Separate)
			Color.Print($"количество пустых строк | строк-комментариев({parameters[Parameter.comment]}) = {lines - filled}", ConsoleColor.Cyan);
		else
			Color.Print($"количество пустых строк = {lines - filled}", ConsoleColor.Cyan);
		Sound.Play(false);
		Console.WriteLine("\nнажмите на любую клавишу, чтобы выйти");
		Console.ReadKey(true);
	}
}